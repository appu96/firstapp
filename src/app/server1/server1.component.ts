import { Component } from '@angular/core';


@Component ({
    selector : 'app-server1',
    templateUrl : './server1.component.html'
})
export class Server1Component {
    allowNewServer = false;
    serverCreationStatus = 'No Server are created';
    serverName = 'TestServer';
    serverCreated = false;
    server1 = ['Testserver', 'Testserver 2'];

    
    constructor(){
        setTimeout(() => {
            this.allowNewServer = true;
        }, 2000);
    }

    onCreateServer() {
        this.serverCreated = true;
        this.server1.push(this.serverName);
        this.serverCreationStatus = 'Server was Created!Name is'  + this.serverName;
    }

    onUpdateServer(event: Event) {
       this.serverName = (<HTMLInputElement>event.target).value;

    }

    

}